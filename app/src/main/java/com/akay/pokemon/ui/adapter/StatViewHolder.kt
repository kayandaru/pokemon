package com.akay.pokemon.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import com.akay.pokemon.data.entity.Item
import com.akay.pokemon.databinding.ItemStatBinding

class StatViewHolder(val mBinding: ItemStatBinding) : RecyclerView.ViewHolder(mBinding.root) {

    fun populateUI(name: String) {
        mBinding.tvItemName.text = name
    }

}