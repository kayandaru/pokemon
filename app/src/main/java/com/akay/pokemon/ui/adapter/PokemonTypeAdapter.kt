package com.akay.pokemon.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akay.pokemon.data.entity.Type
import com.akay.pokemon.databinding.ItemStatBinding

class PokemonTypeAdapter : RecyclerView.Adapter<StatViewHolder>() {

    private val mList: MutableList<Type> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setTypeList(types: List<Type>?) {
        mList.clear()
        if (!types.isNullOrEmpty()) {
            mList.addAll(types)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatViewHolder {
        return StatViewHolder(
            ItemStatBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: StatViewHolder, position: Int) {
        getItem(position)?.let {
            holder.populateUI(it.type.name)
        }
    }

    private fun getItem(position: Int) : Type? {
        return if (mList.isNotEmpty()) mList[position] else null
    }

    override fun getItemCount(): Int {
        return mList.size
    }


}