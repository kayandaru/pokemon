package com.akay.pokemon.ui.adapter

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akay.pokemon.R
import com.akay.pokemon.data.entity.MyPokemon
import com.akay.pokemon.databinding.ItemMyPokemonBinding
import com.akay.pokemon.util.Const
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class MyPokemonAdapter : RecyclerView.Adapter<MyPokemonAdapter.ViewHolder>() {

    private var mList: MutableList<MyPokemon> = mutableListOf()

    companion object {
        var sListener: OnItemClickListener? = null
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setPokemonList(pokemonList: List<MyPokemon>?) {
        mList.clear()
        pokemonList?.let { list ->
            if (list.isNotEmpty()) {
                mList.addAll(list)
            }
        }
        notifyDataSetChanged()
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener) {
        sListener = onItemClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemMyPokemonBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let {
            holder.populateUI(it)
        }
    }

    fun getItem(position: Int): MyPokemon? {
        Log.d(Const.TAG, itemCount.toString())
        Log.d(Const.TAG, mList.toString())

        return if (itemCount > 0) mList[position] else null
    }

    override fun getItemCount(): Int {
        return mList.size
    }


    class ViewHolder(private val mBinding: ItemMyPokemonBinding) :
        RecyclerView.ViewHolder(mBinding.root) {

        init {
            mBinding.root.setOnClickListener {
                sListener?.onItemClicked(it, adapterPosition)
            }
        }

        fun populateUI(pokemon: MyPokemon) {
            mBinding.tvName.text = pokemon.name
            pokemon.artworkUrl?.let { artworkUrl ->
                Glide.with(mBinding.root.context)
                    .load(artworkUrl)
                    .fitCenter()
                    .placeholder(R.drawable.img_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(mBinding.ivImage)
            }

            pokemon.nickname?.let { nickname ->
                mBinding.tvNickname.text = nickname
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClicked(view: View, position: Int)
    }

}