package com.akay.pokemon.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.akay.pokemon.App
import com.akay.pokemon.data.entity.MyPokemon
import com.akay.pokemon.databinding.ActivityPokemonBinding
import com.akay.pokemon.ui.adapter.MyPokemonAdapter
import com.akay.pokemon.util.Const
import com.akay.pokemon.viewmodel.PokemonViewModel
import com.akay.pokemon.viewmodel.PokemonViewModelFactory

class PokemonActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityPokemonBinding

    private val mViewModel : PokemonViewModel by viewModels {
        PokemonViewModelFactory((application as App).repository)
    }
    private lateinit var mErrorLiveData: LiveData<String?>
    private var mPokemonLiveData: LiveData<List<MyPokemon>?>? = null
    private lateinit var mAdapter: MyPokemonAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityPokemonBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        supportActionBar?.let { actionBar ->
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        mErrorLiveData = mViewModel.getError()

        mAdapter = MyPokemonAdapter()
        mAdapter.setOnItemClickListener(object : MyPokemonAdapter.OnItemClickListener {
            override fun onItemClicked(view: View, position: Int) {
                val data = mAdapter.getItem(position)
                data?.let {
                    val intent = Intent(this@PokemonActivity, DetailActivity::class.java)
                    intent.putExtra(Const.EXTRA_ORIGIN, PokemonActivity::class.java.simpleName)
                    intent.putExtra(Const.EXTRA_DATA, it)
                    startActivity(intent)
                }
            }
        })

        initialize()
    }

    private fun initialize() {
        with(mBinding) {
            val lm = GridLayoutManager(this@PokemonActivity, 2, GridLayoutManager.VERTICAL, false)
            rvMypokemon.layoutManager = lm
            rvMypokemon.itemAnimator = DefaultItemAnimator()
            rvMypokemon.setHasFixedSize(true)
            rvMypokemon.isNestedScrollingEnabled = true
            rvMypokemon.adapter = mAdapter
        }
    }

    private fun loadMyPokemonList() {
        mPokemonLiveData = mViewModel.getMyPokemonList()
        mPokemonLiveData?.observe(this@PokemonActivity) {
            mAdapter.setPokemonList(it)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    private fun subscribe() {
        mErrorLiveData.observe(this@PokemonActivity) {
            it?.let { error ->
                Log.d(Const.TAG, error)
            }
        }

        loadMyPokemonList()
    }

    private fun unsubscribe() {
        if (mErrorLiveData.hasObservers()) mErrorLiveData.removeObservers(this@PokemonActivity)

        mPokemonLiveData?.let { liveData ->
            if (liveData.hasObservers()) liveData.removeObservers(this@PokemonActivity)
        }
    }

    override fun onResume() {
        super.onResume()
        subscribe()
    }

    override fun onPause() {
        unsubscribe()
        super.onPause()
    }

}