package com.akay.pokemon.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.akay.pokemon.App
import com.akay.pokemon.R
import com.akay.pokemon.data.entity.MyPokemon
import com.akay.pokemon.databinding.ActivityDetailBinding
import com.akay.pokemon.ui.adapter.PokemonMoveAdapter
import com.akay.pokemon.ui.adapter.PokemonTypeAdapter
import com.akay.pokemon.util.Const
import com.akay.pokemon.util.PokemonDialog
import com.akay.pokemon.viewmodel.PokemonViewModel
import com.akay.pokemon.viewmodel.PokemonViewModelFactory
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy

class DetailActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityDetailBinding
    private var mPokemon: MyPokemon? = null
    private var mOrigin: String? = null
    private var mCaptureRate: Int = 0

    private val mViewModel : PokemonViewModel by viewModels {
        PokemonViewModelFactory((application as App).repository)
    }

    private lateinit var mErrorLiveData: LiveData<String?>
    private lateinit var mTypeAdapter: PokemonTypeAdapter
    private lateinit var mMoveAdapter: PokemonMoveAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        supportActionBar?.let { actionBar ->
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        }

        mPokemon = intent.getParcelableExtra(Const.EXTRA_DATA)
        mOrigin = intent.getStringExtra(Const.EXTRA_ORIGIN)

        //mViewModel = ViewModelProvider(this)[PokemonViewModel::class.java]
        mErrorLiveData = mViewModel.getError()

        mTypeAdapter = PokemonTypeAdapter()
        mMoveAdapter = PokemonMoveAdapter()

        populateUI()
    }

    private fun populateUI() {
        mPokemon?.let { pokemon ->
            with(mBinding) {

                tvName.text = pokemon.name

                tvNickname.text = ""
                pokemon.nickname?.let { nickname ->
                    tvNickname.text = nickname
                }

                Glide.with(root.context)
                    .load(pokemon.artworkUrl)
                    .fitCenter()
                    .placeholder(R.drawable.img_placeholder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivImage)

                content.btnToggleStat.check(R.id.btn_moves)
                content.btnToggleStat.addOnButtonCheckedListener { _, checkedId, checked ->
                    //Log.d(Const.TAG, "checked: $checked")
                    if (checkedId == R.id.btn_moves && checked) {
                        //Log.d(Const.TAG, "checkedId: moves")
                        mBinding.content.rvStat.adapter = mMoveAdapter
                    } else if (checkedId == R.id.btn_types && checked) {
                        //Log.d(Const.TAG, "checkedId: types")
                        mBinding.content.rvStat.adapter = mTypeAdapter
                    }
                }

                renderButton()
            }

            setupRecyclerView()
            getPokemonCaptureRate(pokemon)
            getPokemonInfo(pokemon)
        }
    }

    private fun renderButton() {
        with(mBinding) {

            var textButton = ""
            mOrigin?.let { origin ->
                if (origin == HomeActivity::class.simpleName) {
                    textButton = getString(R.string.catch_pokemon)
                    btnCatch.setOnClickListener { showPopupDialog() }
                } else if (origin == PokemonActivity::class.simpleName) {
                    textButton = getString(R.string.remove_pokemon)
                    btnCatch.setOnClickListener { releasePokemon() }
                }
                btnCatch.text = textButton
            }
        }
    }

    private fun showPopupDialog() {
        mPokemon?.let {
            val dialog = PokemonDialog.newInstance(it, mCaptureRate)
            dialog.setCallback(object: PokemonDialog.Callback {
                override fun onSubmit(dialog: PokemonDialog, myPokemon: MyPokemon) {
                    addMyPokemon(myPokemon)
                }
            })
            dialog.show(supportFragmentManager, "PokemonDialog")
        }
    }

    private fun addMyPokemon(myPokemon: MyPokemon) {
        val insertLiveData = mViewModel.insertMyPokemon(myPokemon)
        insertLiveData.observe(this@DetailActivity) {
            it?.let {
                insertLiveData.removeObservers(this@DetailActivity)
                startActivity(Intent(this@DetailActivity, PokemonActivity::class.java))
                this@DetailActivity.finish()
            }
        }
    }

    private fun releasePokemon() {
        mOrigin?.let { origin ->
            if (origin == PokemonActivity::class.simpleName) {
                mPokemon?.let { myPokemon ->
                    myPokemon.pokemonId?.let {
                        mViewModel.deleteMyPokemon(myPokemon)
                        this@DetailActivity.finish()
                    }
                }
            }
        }
    }

    private fun setupRecyclerView() {
        val lm = GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false)
        with(mBinding) {
            content.rvStat.layoutManager = lm
            content.rvStat.itemAnimator = DefaultItemAnimator()
            content.rvStat.setHasFixedSize(true)
            content.rvStat.isNestedScrollingEnabled = true

            mBinding.content.rvStat.adapter = mMoveAdapter
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    private fun getPokemonInfo(pokemon: MyPokemon) {
        val pokemonLiveData = mViewModel.getPokemonDetail(pokemon.name)
        pokemonLiveData.observe(this@DetailActivity) { data ->
            data?.let {
                pokemonLiveData.removeObservers(this@DetailActivity)
                mMoveAdapter.setMoveList(it.moves)
                mTypeAdapter.setTypeList(it.types)
            }
        }
    }

    private fun getPokemonCaptureRate(pokemon: MyPokemon) {
        val captureRateLiveData = mViewModel.getPokemonCaptureRate(pokemon.name)
        captureRateLiveData.observe(this@DetailActivity) { data ->
            data?.let {
                captureRateLiveData.removeObservers(this@DetailActivity)
                mCaptureRate = it.captureRate
                val captureText = "Catch %: $mCaptureRate"
                mBinding.tvCatchDescription.text = captureText
                mBinding.btnCatch.visibility = View.VISIBLE
            }
        }
    }

    private fun subscribe() {
        // observe error
        mErrorLiveData.observe(this) { it ->
            it?.let { error ->
                Log.e(Const.TAG, error)
            }
        }

        // observe/get mypokemon from local database
    }

    private fun unsubscribe() {
        //unsubscribe error
        if (mErrorLiveData.hasObservers()) mErrorLiveData.removeObservers(this)

        //unsubscribe from mypokemon observer
    }

    override fun onResume() {
        super.onResume()
        subscribe()
    }

    override fun onPause() {
        unsubscribe()
        super.onPause()
    }

}