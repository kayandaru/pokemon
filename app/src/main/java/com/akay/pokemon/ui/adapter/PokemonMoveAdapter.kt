package com.akay.pokemon.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.akay.pokemon.data.entity.Move
import com.akay.pokemon.databinding.ItemStatBinding

class PokemonMoveAdapter  : RecyclerView.Adapter<StatViewHolder>() {

    private val mList: MutableList<Move> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setMoveList(moves: List<Move>?) {
        mList.clear()
        if (!moves.isNullOrEmpty()) {
            mList.addAll(moves)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StatViewHolder {
        return StatViewHolder(
            ItemStatBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: StatViewHolder, position: Int) {
        getItem(position)?.let {
            holder.populateUI(it.move.name)
        }
    }

    private fun getItem(position: Int) : Move? {
        return if (mList.isNotEmpty()) mList[position] else null
    }

    override fun getItemCount(): Int {
        return mList.size
    }
}