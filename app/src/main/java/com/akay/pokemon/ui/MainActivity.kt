package com.akay.pokemon.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import com.akay.pokemon.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        gotoHomeScreen()
    }

    private val mRunnable = Runnable {
        startActivity(Intent(this@MainActivity, HomeActivity::class.java))
        this@MainActivity.finish()
    }

    private fun gotoHomeScreen() {
        Handler(Looper.getMainLooper()).postDelayed(mRunnable, 3000)
    }

}