package com.akay.pokemon.ui

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.akay.pokemon.App
import com.akay.pokemon.R
import com.akay.pokemon.data.api.PokemonListResponse
import com.akay.pokemon.databinding.ActivityHomeBinding
import com.akay.pokemon.ui.adapter.PokemonAdapter
import com.akay.pokemon.util.Const
import com.akay.pokemon.viewmodel.PokemonViewModel
import com.akay.pokemon.viewmodel.PokemonViewModelFactory

class HomeActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivityHomeBinding
    private val mViewModel : PokemonViewModel by viewModels {
        PokemonViewModelFactory((application as App).repository)
    }
    private lateinit var mErrorLiveData: LiveData<String?>
    private var mPokemonLiveData: LiveData<PokemonListResponse?>? = null
    private lateinit var mAdapter: PokemonAdapter

    private var mOffset: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mErrorLiveData = mViewModel.getError()

        mAdapter = PokemonAdapter()
        mAdapter.setOnItemClickListener(object : PokemonAdapter.OnItemClickListener {
            override fun onItemClicked(view: View, position: Int) {
                val data = mAdapter.getItem(position)
                data?.let {
                    val intent = Intent(this@HomeActivity, DetailActivity::class.java)
                    intent.putExtra(Const.EXTRA_ORIGIN, HomeActivity::class.java.simpleName)
                    intent.putExtra(Const.EXTRA_DATA, data)
                    startActivity(intent)
                }
            }
        })

        initialize()
    }

    private fun initialize() {
        with(mBinding) {
            val lm = GridLayoutManager(this@HomeActivity, 2, GridLayoutManager.VERTICAL, false)
            rvHome.layoutManager = lm
            rvHome.itemAnimator = DefaultItemAnimator()
            rvHome.setHasFixedSize(true)
            rvHome.isNestedScrollingEnabled = true
            rvHome.adapter = mAdapter
        }
    }

    private fun loadPokemonList() {
        mPokemonLiveData = mViewModel.getPokemonList(mOffset)
        mPokemonLiveData?.observe(this@HomeActivity) {
            mAdapter.setPokemonList(it?.results)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.menu_action_mypokemon) {
            startActivity(Intent(this@HomeActivity, PokemonActivity::class.java))
            true
        } else {
            super.onOptionsItemSelected(item)
        }
    }

    private fun subscribe() {
        mErrorLiveData.observe(this@HomeActivity) {
            it?.let { error ->
                Log.d(Const.TAG, error)
            }
        }

        loadPokemonList()
    }

    private fun unsubscribe() {
        if (mErrorLiveData.hasObservers()) mErrorLiveData.removeObservers(this@HomeActivity)

        mPokemonLiveData?.let { pokemonLiveData ->
            if (pokemonLiveData.hasObservers()) pokemonLiveData.removeObservers(this@HomeActivity)
        }
    }

    override fun onResume() {
        super.onResume()
        subscribe()
    }

    override fun onPause() {
        unsubscribe()
        super.onPause()
    }

}