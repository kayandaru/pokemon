package com.akay.pokemon

import androidx.multidex.MultiDexApplication
import com.akay.pokemon.data.AppDatabase
import com.akay.pokemon.data.repository.PokemonRepository

class App : MultiDexApplication() {

    private val database by lazy {
        AppDatabase.getDatabase(this)
    }

    val repository by lazy {
        PokemonRepository(database.pokemonDao())
    }

    companion object {
        @get:Synchronized
        lateinit var context: App
            private set
    }

    override fun onCreate() {
        super.onCreate()
        context = this
    }

}