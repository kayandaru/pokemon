package com.akay.pokemon.viewmodel

import androidx.lifecycle.*
import com.akay.pokemon.data.api.PokemonListResponse
import com.akay.pokemon.data.entity.MyPokemon
import com.akay.pokemon.data.entity.Pokemon
import com.akay.pokemon.data.entity.Species
import com.akay.pokemon.data.repository.PokemonRepository
import kotlinx.coroutines.launch
import java.lang.IllegalArgumentException

class PokemonViewModel(private val repository: PokemonRepository) : ViewModel() {

    private val lastInsertId: MutableLiveData<Long?> = MutableLiveData()

    init {
        lastInsertId.value = null
    }

    fun getPokemonList(offset: Int) : LiveData<PokemonListResponse?> {
        return repository.getPokemonList(offset)
    }

    fun getPokemonDetail(name: String) : LiveData<Pokemon?> {
        return repository.getPokemonDetail(name)
    }

    fun getPokemonCaptureRate(name: String) : LiveData<Species?> {
        return repository.getPokemonCaptureRate(name)
    }

    //Local Repository

    fun getMyPokemonList() : LiveData<List<MyPokemon>?> {
        return repository.getMyPokemonList()
    }

    fun insertMyPokemon(pokemon: MyPokemon) : LiveData<Long?> {
        lastInsertId.value = null
        insertMyPokemonReturnId(pokemon)
        return lastInsertId
    }

    private fun insertMyPokemonReturnId(pokemon: MyPokemon) = viewModelScope.launch {
        val pokemonId = repository.insertMyPokemon(pokemon)
        lastInsertId.value = pokemonId
    }

    fun deleteMyPokemon(pokemon: MyPokemon) = viewModelScope.launch {
        repository.deleteMyPokemon(pokemon.pokemonId!!)
    }

    fun getError() : LiveData<String?> {
        return repository.error
    }

}

class PokemonViewModelFactory(private val repository: PokemonRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PokemonViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return PokemonViewModel(repository) as T
        }
        throw IllegalArgumentException("unknown ViewModel class")
    }
}