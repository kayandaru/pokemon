package com.akay.pokemon.data.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Move(
    val move: Item
) : Parcelable {

    override fun toString(): String {
        return "Move(move=$move)"
    }

}