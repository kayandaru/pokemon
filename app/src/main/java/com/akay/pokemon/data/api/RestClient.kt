package com.akay.pokemon.data.api

import androidx.multidex.BuildConfig
import com.akay.pokemon.util.Const
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.net.CookieHandler
import java.net.CookieManager
import java.net.CookiePolicy

object RestClient {

    private fun okHttpClient(): OkHttpClient {
        CookieHandler.setDefault(CookieManager(null, CookiePolicy.ACCEPT_ALL))
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)
        return OkHttpClient.Builder()
            .followRedirects(true)
            .followSslRedirects(true)
            .retryOnConnectionFailure(true)
            .addInterceptor(loggingInterceptor)
            .build()
    }

    //Some apps might have several hosts or target url, another method could be set with dynamic
    //url params if needed. In this test, I assume we only have one target/host only
    private fun getRetrofit(): Retrofit {
        val gson = GsonBuilder().setLenient().create()
        return Retrofit.Builder()
            .baseUrl(Const.API_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(okHttpClient())
            .build()
    }

    fun getService(): PokemonService {
        return getRetrofit().create(PokemonService::class.java)
    }

}