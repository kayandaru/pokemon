package com.akay.pokemon.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Species(
    @SerializedName("capture_rate") val captureRate: Int
) : Parcelable {

    override fun toString(): String {
        return "Species(captureRate=$captureRate)"
    }
}
