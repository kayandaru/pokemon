package com.akay.pokemon.data.api

import android.os.Parcelable
import com.akay.pokemon.data.entity.MyPokemon
import kotlinx.parcelize.Parcelize

@Parcelize
data class PokemonListResponse(
    var count: Long?,
    var next: String?,
    var previous: String?,
    val results: List<MyPokemon>
) : Parcelable {

    override fun toString(): String {
        return "PokemonListResponse(count=$count, next=$next, previous=$previous, results=$results)"
    }
}
