package com.akay.pokemon.data.repository

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.akay.pokemon.data.api.PokemonListResponse
import com.akay.pokemon.data.api.PokemonService
import com.akay.pokemon.data.api.RestClient
import com.akay.pokemon.data.dao.PokemonDao
import com.akay.pokemon.data.entity.MyPokemon
import com.akay.pokemon.data.entity.Pokemon
import com.akay.pokemon.data.entity.Species
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PokemonRepository(private val dao: PokemonDao) {

    private val service: PokemonService = RestClient.getService()
    val error: MutableLiveData<String?> = MutableLiveData()

    fun getPokemonList(offset: Int): MutableLiveData<PokemonListResponse?> {
        val liveData: MutableLiveData<PokemonListResponse?> = MutableLiveData()
        error.value = null

        val call = service.getPokemonList(offset)
        call.enqueue(object : Callback<PokemonListResponse> {
            override fun onResponse(
                call: Call<PokemonListResponse>,
                response: Response<PokemonListResponse>
            ) {
                if (response.isSuccessful) {
                    liveData.value = response.body()
                }
            }

            override fun onFailure(call: Call<PokemonListResponse>, t: Throwable) {
                error.value = t.message
            }
        })


        return liveData
    }

    fun getPokemonDetail(name: String) : MutableLiveData<Pokemon?> {
        val liveData: MutableLiveData<Pokemon?> = MutableLiveData()
        error.value = null

        val call = service.getPokemonDetail(name)
        call.enqueue(object: Callback<Pokemon> {
            override fun onResponse(call: Call<Pokemon>, response: Response<Pokemon>) {
                if (response.isSuccessful) {
                    liveData.value = response.body()
                }
            }

            override fun onFailure(call: Call<Pokemon>, t: Throwable) {
                error.value = t.message
            }
        })

        return liveData
    }

    fun getPokemonCaptureRate(name: String) : MutableLiveData<Species?> {
        val liveData: MutableLiveData<Species?> = MutableLiveData()
        error.value = null

        val call = service.getPokemonCaptureRate(name)
        call.enqueue(object: Callback<Species> {
            override fun onResponse(call: Call<Species>, response: Response<Species>) {
                if (response.isSuccessful) {
                    liveData.value = response.body()
                }
            }

            override fun onFailure(call: Call<Species>, t: Throwable) {
                error.value = t.message
            }
        })

        return liveData
    }

    fun getMyPokemonList() : LiveData<List<MyPokemon>?> {
        return dao.getMyPokemonList()
    }

    @WorkerThread
    suspend fun insertMyPokemon(pokemon: MyPokemon) : Long {
        return dao.insertMyPokemon(pokemon)
    }

    @WorkerThread
    suspend fun deleteMyPokemon(pokemonId: Long) {
        dao.deleteMyPokemon(pokemonId)
    }

}