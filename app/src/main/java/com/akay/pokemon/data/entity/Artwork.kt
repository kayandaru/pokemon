package com.akay.pokemon.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Artwork(
    @SerializedName("official-artwork")
    val officialArtwork: String
) : Parcelable {

    override fun toString(): String {
        return "Artwork(officialArtwork='$officialArtwork')"
    }

}