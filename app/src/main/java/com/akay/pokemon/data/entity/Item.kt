package com.akay.pokemon.data.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Item(
    val name: String,
    val url: String
) : Parcelable {

    override fun toString(): String {
        return "Item(name='$name', url='$url')"
    }

}