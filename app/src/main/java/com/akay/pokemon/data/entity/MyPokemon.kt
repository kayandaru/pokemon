package com.akay.pokemon.data.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.akay.pokemon.util.Const
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
class MyPokemon(
    @PrimaryKey(autoGenerate = true) val pokemonId: Long? = null,
    var id: Long? = null,
    val name: String,
    val url: String,
    var nickname: String? = null,
    var artworkUrl: String? = null
): Parcelable {

    @Ignore
    fun generatePokemonId(url: String?) : Long? {
        url?.let {
            val endpoint: String = Const.API_URL + "pokemon/"
            return url.substring(endpoint.length, (url.length - 1)).toLong()
        }
        return null
    }

    @Ignore
    fun generateArtworkUrl(url: String?) : String? {
        generatePokemonId(url)?.let {
            return Const.ARTWORK_URL + it.toString() + ".png"
        }
        return null
    }

    override fun toString(): String {
        return "MyPokemon(_id=$pokemonId, id=$id, name='$name', url='$url', nickname=$nickname, artworkUrl=$artworkUrl)"
    }

}