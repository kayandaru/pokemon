package com.akay.pokemon.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.akay.pokemon.data.entity.MyPokemon

@Dao
interface PokemonDao {

    @Query("SELECT * FROM mypokemon")
    fun getMyPokemonList() : LiveData<List<MyPokemon>?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMyPokemon(myPokemon: MyPokemon): Long

    @Query("DELETE FROM mypokemon WHERE pokemonId = :pokemonId")
    suspend fun deleteMyPokemon(pokemonId: Long)

}