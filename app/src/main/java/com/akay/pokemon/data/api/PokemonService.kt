package com.akay.pokemon.data.api

import com.akay.pokemon.data.entity.Pokemon
import com.akay.pokemon.data.entity.Species
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PokemonService {

    @GET("pokemon?limit=30")
    fun getPokemonList(@Query("offset") offset: Int) : Call<PokemonListResponse>

    @GET("pokemon/{name}")
    fun getPokemonDetail(@Path("name") name: String) : Call<Pokemon>

    @GET("pokemon-species/{name}")
    fun getPokemonCaptureRate(@Path("name") name: String) : Call<Species>

}