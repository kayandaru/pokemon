package com.akay.pokemon.data.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Pokemon(
    val id: Long,
    val name: String,
    val moves: List<Move>,
    val types: List<Type>
) : Parcelable {

    override fun toString(): String {
        return "Pokemon(id=$id, name='$name', moves=$moves, types=$types)"
    }
}
