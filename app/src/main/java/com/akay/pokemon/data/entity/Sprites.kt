package com.akay.pokemon.data.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Sprites(
    val other: Artwork
) : Parcelable {

    override fun toString(): String {
        return "Sprites(other=$other)"
    }
}