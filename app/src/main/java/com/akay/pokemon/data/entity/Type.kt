package com.akay.pokemon.data.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Type(
    val slot: Int,
    val type: Item
) : Parcelable {

    override fun toString(): String {
        return "Type(slot=$slot, type=$type)"
    }
}