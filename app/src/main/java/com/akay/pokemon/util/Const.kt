package com.akay.pokemon.util

object Const {

    const val TAG = "PokemonApp"
    const val API_URL = "https://pokeapi.co/api/v2/"
    const val ARTWORK_URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/"

    const val EXTRA_ORIGIN = "pokemonapp.extra_origin"
    const val EXTRA_DATA = "pokemonapp.extra_data"

}