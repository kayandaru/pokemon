package com.akay.pokemon.util

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import com.akay.pokemon.R
import com.akay.pokemon.data.entity.MyPokemon
import com.akay.pokemon.databinding.DialogPokemonBinding

class PokemonDialog  : DialogFragment() {

    private lateinit var mBinding: DialogPokemonBinding
    private var mPokemon: MyPokemon? = null
    private var mCaptureRate: Int? = null

    companion object {
        private var sCallback: Callback? = null

        private const val EXT_MY_POKEMON = "ext_mypokemon"
        private const val EXT_CAPTURE_RATE = "ext_capture_rate"

        @JvmStatic
        fun newInstance(myPokemon: MyPokemon, captureRate: Int) = PokemonDialog().apply {
            arguments = bundleOf(
                EXT_MY_POKEMON to myPokemon,
                EXT_CAPTURE_RATE to captureRate
            )
        }
    }

    fun setCallback(callback: Callback) {
        sCallback = callback
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mBinding = DialogPokemonBinding.inflate(inflater, container, false)

        return mBinding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog: Dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            mPokemon = it.getParcelable(EXT_MY_POKEMON)
            mCaptureRate = it.getInt(EXT_CAPTURE_RATE)
        }

        mCaptureRate?.let { rate ->
            if (rate >= 50) {
                mBinding.tvInfo.text = getString(R.string.capture_success)
                mBinding.tilNickname.visibility = View.VISIBLE
                mBinding.btnAdd.visibility = View.VISIBLE
            } else {
                mBinding.tvInfo.text = getString(R.string.capture_failed)
                mBinding.tilNickname.visibility = View.GONE
                mBinding.btnAdd.visibility = View.GONE
            }
        }

        mBinding.btnClose.setOnClickListener{ this@PokemonDialog.dismiss() }
        mBinding.btnAdd.setOnClickListener { submitNickname() }
    }

    private fun submitNickname() {
        val text = mBinding.etNickname.text.toString()
        if (text.isEmpty()) {
            mBinding.etNickname.error = "Nickname cannot be empty"
        } else {
            mPokemon?.let {
                it.nickname = text
                sCallback?.onSubmit(this, it)
            }
            this.dismiss()
        }
    }

    interface Callback {
        fun onSubmit(dialog: PokemonDialog, myPokemon: MyPokemon)
    }

}